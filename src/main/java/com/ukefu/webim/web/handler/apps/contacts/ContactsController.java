package com.ukefu.webim.web.handler.apps.contacts;

import static org.elasticsearch.index.query.QueryBuilders.termQuery;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.util.PinYinTools;
import com.ukefu.util.UKTools;
import com.ukefu.util.task.DSData;
import com.ukefu.util.task.DSDataEvent;
import com.ukefu.util.task.ExcelImportProecess;
import com.ukefu.util.task.export.ExcelExporterProcess;
import com.ukefu.util.task.process.ContactsProcess;
import com.ukefu.webim.service.es.ContactsRepository;
import com.ukefu.webim.service.repository.AgentUserContactsRepository;
import com.ukefu.webim.service.repository.ContactsItemAuthorizeRepository;
import com.ukefu.webim.service.repository.ContactsItemRepository;
import com.ukefu.webim.service.repository.MetadataRepository;
import com.ukefu.webim.service.repository.OrganRepository;
import com.ukefu.webim.service.repository.PropertiesEventRepository;
import com.ukefu.webim.service.repository.ReporterRepository;
import com.ukefu.webim.service.repository.UserRepository;
import com.ukefu.webim.util.PropertiesEventUtils;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.AgentUserContacts;
import com.ukefu.webim.web.model.Contacts;
import com.ukefu.webim.web.model.ContactsItem;
import com.ukefu.webim.web.model.ContactsItemAuthorize;
import com.ukefu.webim.web.model.MetadataTable;
import com.ukefu.webim.web.model.Organ;
import com.ukefu.webim.web.model.PropertiesEvent;
import com.ukefu.webim.web.model.UKeFuDic;
import com.ukefu.webim.web.model.User;

@Controller
@RequestMapping("/apps/contacts")
public class ContactsController extends Handler{
	
	@Autowired
	private ContactsRepository contactsRes ;
	
	@Autowired
	private PropertiesEventRepository propertiesEventRes ;
	
	@Autowired
	private ReporterRepository reporterRes ;
	
	@Autowired
	private MetadataRepository metadataRes ;
	
	@Autowired
	private AgentUserContactsRepository agentUserContactsRes; 
	
	@Autowired
	private ContactsItemRepository contactsItemRes ;
	
	@Autowired
	private ContactsItemAuthorizeRepository contactsItemAuthorizeRes ;
	
	@Autowired
	private OrganRepository organRes ;	//查询部门
	
	@Autowired
	private UserRepository userRes;
	
	@Value("${web.upload-path}")
    private String path;
	
    @RequestMapping("/index")
    @Menu(type = "contacts" , subtype = "index" ,name="index")
    public ModelAndView index(ModelMap map , HttpServletRequest request , @Valid String q , @Valid String ckind, @Valid String itemid) {
    	map.addAttribute("contactsList", contactsRes.findByQueryAndOrgi(null,super.getOrgi(request), null , null , false, this.doIndex(map, request, q, ckind, itemid) ,q , new PageRequest(super.getP(request) , super.getPs(request)))) ;
    	return request(super.createAppsTempletResponse("/apps/business/contacts/index"));
    }
    
    public BoolQueryBuilder doIndex(ModelMap map , HttpServletRequest request , String q , String ckind, String itemid) {
    	BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
    	if(!StringUtils.isBlank(q)){
        	map.put("q", q) ;
        }
    	if(!StringUtils.isBlank(ckind) && !ckind.equals("null")){
    		boolQueryBuilder.must(termQuery("ckind" , ckind)) ;
        	map.put("ckind", ckind) ;
        	map.put("ckinddic", UKeFuDic.getInstance().getDicItem(ckind)) ;
        }
    	
    	//得到有权限的项目
    	List<ContactsItem> contactsItemList = getContactsItem(super.getUser(request),super.getOrgi(request)) ;
    	map.addAttribute("contactsItemList", contactsItemList) ;
    	
    	if(!StringUtils.isBlank(itemid) && !itemid.equals("null")){
    		ContactsItem contactsItem = contactsItemRes.findByIdAndOrgi(itemid, super.getOrgi(request)) ;
    		if (contactsItem != null) {
				boolQueryBuilder.must(termQuery("itemid",contactsItem.getId()));
				ContactsItemAuthorize authorize = contactsItemAuthorizeRes.findByItemidAndUseridAndOrgiAndDatastatus(itemid, super.getUser(request).getId(), super.getOrgi(request),false);
				if ((authorize != null && authorize.isSup()) || contactsItem.getCreater().equals(super.getUser(request).getId())) {
					map.addAttribute("isSup",true);
				}
			}
        	map.addAttribute("contactsItem", contactsItem);
        	map.put("itemid", itemid) ;
        }else {
        	boolQueryBuilder.mustNot(QueryBuilders.wildcardQuery("itemid", "*"));
		}
    	
    	//搜索条件
    	if(!StringUtils.isBlank(request.getParameter("name"))) {
    		boolQueryBuilder.must(new BoolQueryBuilder().should(QueryBuilders.matchPhraseQuery("name",request.getParameter("name"))).should(QueryBuilders.wildcardQuery("name","*"+request.getParameter("name")+"*"))) ;
			map.put("name", request.getParameter("name")) ;
		}
    	if(!StringUtils.isBlank(request.getParameter("creater"))) {
    		boolQueryBuilder.must(termQuery("creater", request.getParameter("creater"))) ;
			map.put("creater", request.getParameter("creater")) ;
		}
    	if(!StringUtils.isBlank(request.getParameter("ckinds"))) {
    		boolQueryBuilder.must(termQuery("ckind", request.getParameter("ckinds"))) ;
			map.put("ckinds", request.getParameter("ckinds")) ;
		}
    	if(!StringUtils.isBlank(request.getParameter("gender"))) {
    		boolQueryBuilder.must(termQuery("gender", request.getParameter("gender"))) ;
			map.put("gender", request.getParameter("gender")) ;
		}
    	if(!StringUtils.isBlank(request.getParameter("phone"))) {
    		boolQueryBuilder.must(termQuery("phone", request.getParameter("phone"))) ;
			map.put("phone", request.getParameter("phone")) ;
		}
    	if(!StringUtils.isBlank(request.getParameter("mobileno"))) {
    		boolQueryBuilder.must(termQuery("mobileno", request.getParameter("mobileno"))) ;
			map.put("mobileno", request.getParameter("mobileno")) ;
		}
    	if(!StringUtils.isBlank(request.getParameter("email"))) {
    		boolQueryBuilder.must(new BoolQueryBuilder().should(QueryBuilders.matchPhraseQuery("email",request.getParameter("email"))).should(QueryBuilders.wildcardQuery("email","*"+request.getParameter("email")+"*"))) ;
			map.put("email", request.getParameter("email")) ;
		}
    	if(!StringUtils.isBlank(request.getParameter("address"))) {
    		boolQueryBuilder.must(new BoolQueryBuilder().should(QueryBuilders.matchPhraseQuery("address",request.getParameter("address"))).should(QueryBuilders.wildcardQuery("address","*"+request.getParameter("address")+"*"))) ;
			map.put("address", request.getParameter("address")) ;
		}
    	if(!StringUtils.isBlank(request.getParameter("province"))) {
    		boolQueryBuilder.must(termQuery("province", request.getParameter("province"))) ;
			map.put("province", request.getParameter("province")) ;
		}
    	if(!StringUtils.isBlank(request.getParameter("city"))) {
    		boolQueryBuilder.must(termQuery("city", request.getParameter("city"))) ;
    		map.put("city", request.getParameter("city")) ;
    	}
    	if(!StringUtils.isBlank(request.getParameter("ctype"))) {
    		boolQueryBuilder.must(termQuery("ctype", request.getParameter("ctype"))) ;
    		map.put("ctype", request.getParameter("ctype")) ;
    	}
    	
    	RangeQueryBuilder rangeQuery = null ;
		//创建时间区间查询
		if(!StringUtils.isBlank(request.getParameter("touchtimebegin")) || !StringUtils.isBlank(request.getParameter("touchtimeend"))){
			if(!StringUtils.isBlank(request.getParameter("touchtimebegin"))) {
				try {
					rangeQuery = QueryBuilders.rangeQuery("touchtime").from(UKTools.dateFormate.parse(request.getParameter("touchtimebegin")).getTime()) ;
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			if(!StringUtils.isBlank(request.getParameter("touchtimeend")) ) {
				try {
					if(rangeQuery == null) {
						rangeQuery = QueryBuilders.rangeQuery("touchtime").to(UKTools.dateFormate.parse(request.getParameter("touchtimeend")).getTime()) ;
					}else {
						rangeQuery.to(UKTools.dateFormate.parse(request.getParameter("touchtimeend")).getTime()) ;
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			map.put("touchtimebegin", request.getParameter("touchtimebegin")) ;
			map.put("touchtimeend", request.getParameter("touchtimeend")) ;
		}
		if(rangeQuery!=null) {
			boolQueryBuilder.must(rangeQuery) ;
		}
    	return boolQueryBuilder ;
	}
    
    public List<ContactsItem> getContactsItem(final User user,final String orgi) {
    	List<ContactsItem> allItemList = contactsItemRes.findByOrgiOrderByCreatetimeDesc(orgi);
    	if (user.isSuperuser()) {
			return allItemList ;
		}else {
			List<ContactsItem> contactsItemList = new ArrayList<ContactsItem>();
			List<ContactsItemAuthorize> authList = contactsItemAuthorizeRes.findByUseridAndOrgiAndDatastatus(user.getId(),orgi ,false) ;
			List<String> parentList = new ArrayList<String>();
			if (allItemList != null && allItemList.size() > 0) {
				for(ContactsItem item : allItemList) {
					if (item.getCreater().equals(user.getId())) {//自己创建的直接放入
						contactsItemList.add(item) ;
					}else {
						if (authList != null && authList.size() > 0) {//判断授权
							for(ContactsItemAuthorize authorize : authList) {
								if (item.getId().equals(authorize.getItemid()) && !item.getCreater().equals(user.getId())) {//若权限表有此项目且不是自己创建
									contactsItemList.add(item) ;//先把项目放入
									for(ContactsItem itemau : allItemList) {
										//判断是否有子项 || 判断是否有父级
										if ((!StringUtils.isBlank(itemau.getParentid()) && !item.getCreater().equals(user.getId()) && item.getId().equals(itemau.getParentid())) || (!StringUtils.isBlank(item.getParentid()) && !item.getCreater().equals(user.getId()) && itemau.getId().equals(item.getParentid()))) {
											if (!parentList.contains(itemau.getId())) {
												parentList.add(itemau.getId());
												contactsItemList.add(itemau) ;//有放入
											}
										}
									}
								}
							}
						}
					}
				}
			}
			parentList.clear();
			parentList = null;
			return contactsItemList ;
		}
	}
    
    @RequestMapping("/today")
    @Menu(type = "contacts" , subtype = "today", name = "today")
    public ModelAndView today(ModelMap map , HttpServletRequest request , @Valid String q , @Valid String ckind, @Valid String itemid) {
    	map.addAttribute("contactsList", contactsRes.findByQueryAndOrgi(null,super.getOrgi(request),UKTools.getStartTime() , null , false, this.doIndex(map, request, q, ckind, itemid) ,q , new PageRequest(super.getP(request) , super.getPs(request)))) ;
    	return request(super.createAppsTempletResponse("/apps/business/contacts/index"));
    }
    
    @RequestMapping("/week")
    @Menu(type = "contacts" , subtype = "week", name = "week")
    public ModelAndView week(ModelMap map , HttpServletRequest request , @Valid String q , @Valid String ckind, @Valid String itemid) {
    	map.addAttribute("contactsList", contactsRes.findByQueryAndOrgi(null,super.getOrgi(request), UKTools.getWeekStartTime() , null , false, this.doIndex(map, request, q, ckind, itemid) ,q , new PageRequest(super.getP(request) , super.getPs(request)))) ;
    	return request(super.createAppsTempletResponse("/apps/business/contacts/index"));
    }
    
    @RequestMapping("/creater")
    @Menu(type = "contacts" , subtype = "creater",name="creater")
    public ModelAndView creater(ModelMap map , HttpServletRequest request , @Valid String q , @Valid String ckind, @Valid String itemid) {
    	BoolQueryBuilder boolQueryBuilder = this.doIndex(map, request, q, ckind, itemid) ;
    	boolQueryBuilder.must(termQuery("creater", super.getUser(request).getId())) ;
    	map.addAttribute("contactsList", contactsRes.findByQueryAndOrgi(null,super.getOrgi(request), null , null , false, boolQueryBuilder ,q , new PageRequest(super.getP(request) , super.getPs(request)))) ;
        return request(super.createAppsTempletResponse("/apps/business/contacts/index"));
    }
    
    @RequestMapping("/delete")
    @Menu(type = "contacts" , subtype = "contacts")
    public ModelAndView delete(HttpServletRequest request ,@Valid Contacts contacts ,@Valid String p,@Valid String ckind,@Valid String itemid) {
    	if(contacts!=null){
    		contacts = contactsRes.findOne(contacts.getId()) ;
    		contacts.setDatastatus(true);							//客户和联系人都是 逻辑删除
    		contactsRes.save(contacts) ;
    		List<AgentUserContacts> agentUserContactsList = agentUserContactsRes.findByContactsidAndOrgi(contacts.getId(), super.getOrgi(request)) ;
    		agentUserContactsRes.delete(agentUserContactsList);
    		
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/apps/contacts/index.html?p="+p+"&ckind="+ckind+"&itemid="+itemid));
    }
    
    @RequestMapping("/add")
    @Menu(type = "contacts" , subtype = "add")
    public ModelAndView add(ModelMap map , HttpServletRequest request,@Valid String ckind,@Valid String itemid) {
    	map.addAttribute("itemid",itemid);
    	map.addAttribute("ckind",ckind);
        return request(super.createRequestPageTempletResponse("/apps/business/contacts/add"));
    }
    
    @RequestMapping(  "/save")
    @Menu(type = "contacts" , subtype = "save")
    public ModelAndView save(HttpServletRequest request  , @Valid Contacts contacts) {
		contacts.setCreater(super.getUser(request).getId());
		contacts.setOrgi(super.getOrgi(request));
		contacts.setOrgan(super.getUser(request).getOrgan());
		contacts.setPinyin(PinYinTools.getInstance().getFirstPinYin(contacts.getName()));
		if(StringUtils.isBlank(contacts.getCbirthday())) {
			contacts.setCbirthday(null);
		}else {
			contacts.setCbirthday(contacts.getCbirthday());
		}
		contactsRes.save(contacts) ;
		String reqString = !StringUtils.isBlank(contacts.getItemid())?"?itemid="+contacts.getItemid():!StringUtils.isBlank(contacts.getCkind())?"?ckind="+contacts.getCkind():"" ;
		return request(super.createRequestPageTempletResponse("redirect:/apps/contacts/index.html"+reqString));
    }
    
    @RequestMapping("/edit")
    @Menu(type = "contacts" , subtype = "contacts")
    public ModelAndView edit(ModelMap map , HttpServletRequest request , @Valid String id,@Valid String ckind,@Valid String itemid) {
    	map.addAttribute("contacts", contactsRes.findOne(id)) ;
    	map.addAttribute("type", itemid) ;
    	map.addAttribute("ckind",ckind);
        return request(super.createRequestPageTempletResponse("/apps/business/contacts/edit"));
    }
    
    @RequestMapping("/detail")
    @Menu(type = "contacts" , subtype = "contacts")
    public ModelAndView detail(ModelMap map , HttpServletRequest request , @Valid String id) {
    	map.addAttribute("contacts", contactsRes.findOne(id)) ;
        return request(super.createRequestPageTempletResponse("/apps/business/contacts/detail"));
    }
    
    
    @RequestMapping(  "/update")
    @Menu(type = "contacts" , subtype = "contacts")
    public ModelAndView update(HttpServletRequest request  , @Valid Contacts contacts,@Valid String type) {
    	Contacts data = contactsRes.findOne(contacts.getId()) ;
    	if(data!=null){
	    	List<PropertiesEvent> events = PropertiesEventUtils.processPropertiesModify(request, contacts , data , "id" , "orgi" , "creater" ,"createtime" , "updatetime") ;	//记录 数据变更 历史
	    	if(events.size()>0){
	    		String modifyid = UKTools.getUUID() ;
	    		Date modifytime = new Date();
	    		for(PropertiesEvent event : events){
	    			event.setDataid(contacts.getId());
	    			event.setCreater(super.getUser(request).getId());
	    			event.setOrgi(super.getOrgi(request));
	    			event.setModifyid(modifyid);
	    			event.setCreatetime(modifytime);
	    			propertiesEventRes.save(event) ;
	    		}
	    	}
	    	contacts.setItemid(data.getItemid());
	    	contacts.setCkind(data.getCkind());
	    	contacts.setCreater(data.getCreater());
	    	contacts.setCreatetime(data.getCreatetime());
	    	contacts.setOrgi(super.getOrgi(request));
	    	contacts.setOrgan(super.getUser(request).getOrgan());
	    	contacts.setPinyin(PinYinTools.getInstance().getFirstPinYin(contacts.getName()));
	    	if(StringUtils.isBlank(contacts.getCbirthday())) {
				contacts.setCbirthday(null);
			}else {
				contacts.setCbirthday(contacts.getCbirthday());
			}
	    	contactsRes.save(contacts);
    	}
    	String reqString = !StringUtils.isBlank(contacts.getItemid())?"?itemid="+contacts.getItemid():!StringUtils.isBlank(contacts.getCkind())?"?ckind="+contacts.getCkind():"" ;
        return request(super.createRequestPageTempletResponse("redirect:/apps/contacts/index.html"+reqString));
    }
    
    @RequestMapping("/imp")
    @Menu(type = "contacts" , subtype = "contacts")
    public ModelAndView imp(ModelMap map , HttpServletRequest request,@Valid String ckind,@Valid String itemid) {
//    	map.addAttribute("ckind",ckind);
    	map.addAttribute("itemid",itemid);
        return request(super.createRequestPageTempletResponse("/apps/business/contacts/imp"));
    }
    
    @RequestMapping("/impsave")
    @Menu(type = "contacts" , subtype = "contacts")
    public ModelAndView impsave(ModelMap map , HttpServletRequest request , @RequestParam(value = "cusfile", required = false) MultipartFile cusfile,@Valid String ckind,@Valid String itemid) throws IOException {
    	DSDataEvent event = new DSDataEvent();
    	String fileName = "contacts/"+UKTools.getUUID()+cusfile.getOriginalFilename().substring(cusfile.getOriginalFilename().lastIndexOf(".")) ;
    	File excelFile = new File(path , fileName) ;
    	if(!excelFile.getParentFile().exists()){
    		excelFile.getParentFile().mkdirs() ;
    	}
    	MetadataTable table = metadataRes.findByTablenameIgnoreCase("uk_contacts") ;
    	if(table!=null){
	    	FileUtils.writeByteArrayToFile(new File(path , fileName), cusfile.getBytes());
	    	event.setDSData(new DSData(table,excelFile , cusfile.getContentType(), super.getUser(request)));
	    	event.getDSData().setClazz(Contacts.class);
	    	event.getDSData().setProcess(new ContactsProcess(contactsRes));
	    	event.setOrgi(super.getOrgi(request));
	    	if(!StringUtils.isBlank(ckind)){
	    		event.getValues().put("ckind", ckind) ;
	    	}
	    	if(!StringUtils.isBlank(itemid)){
	    		event.getValues().put("itemid", itemid) ;
	    	}
	    	event.getValues().put("creater", super.getUser(request).getId()) ;
	    	reporterRes.save(event.getDSData().getReport()) ;
	    	new ExcelImportProecess(event).process() ;		//启动导入任务
    	}
    	
    	return request(super.createRequestPageTempletResponse("redirect:/apps/contacts/index.html?itemid="+itemid+"&ckind="+ckind));
    }
    
    @RequestMapping("/expids")
    @Menu(type = "contacts" , subtype = "contacts")
    public void expids(ModelMap map , HttpServletRequest request , HttpServletResponse response , @Valid String[] ids) throws IOException {
    	if(ids!=null && ids.length > 0){
    		Iterable<Contacts> contactsList = contactsRes.findAll(Arrays.asList(ids)) ;
    		MetadataTable table = metadataRes.findByTablenameIgnoreCase("uk_contacts") ;
    		List<Map<String,Object>> values = new ArrayList<Map<String,Object>>();
    		for(Contacts contacts : contactsList){
    			values.add(UKTools.transBean2Map(contacts)) ;
    		}
    		
    		response.setHeader("content-disposition", "attachment;filename=UCKeFu-Contacts-"+new SimpleDateFormat("yyyy-MM-dd").format(new Date())+".xls");  
    		
    		ExcelExporterProcess excelProcess = new ExcelExporterProcess( values, table, response.getOutputStream()) ;
    		excelProcess.process();
    	}
    	
        return ;
    }
    
    @RequestMapping("/expall")
    @Menu(type = "contacts" , subtype = "contacts")
    public void expall(ModelMap map , HttpServletRequest request , HttpServletResponse response) throws IOException {
    	BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
    	boolQueryBuilder.must(termQuery("datastatus" , false)) ;		//只导出 数据删除状态 为 未删除的 数据
    	Iterable<Contacts> contactsList = contactsRes.findByQueryAndOrgi(null, super.getOrgi(request),null , null , false, boolQueryBuilder , null , new PageRequest(super.getP(request) , 10000));
    	
    	MetadataTable table = metadataRes.findByTablenameIgnoreCase("uk_contacts") ;
		List<Map<String,Object>> values = new ArrayList<Map<String,Object>>();
		for(Contacts contacts : contactsList){
			values.add(UKTools.transBean2Map(contacts)) ;
		}
		
		response.setHeader("content-disposition", "attachment;filename=UCKeFu-Contacts-"+new SimpleDateFormat("yyyy-MM-dd").format(new Date())+".xls");  
		
		ExcelExporterProcess excelProcess = new ExcelExporterProcess( values, table, response.getOutputStream()) ;
		excelProcess.process();
        return ;
    }
    
    @RequestMapping("/expsearch")
    @Menu(type = "contacts" , subtype = "contacts")
    public void expall(ModelMap map , HttpServletRequest request , HttpServletResponse response , @Valid String q , @Valid String ekind) throws IOException {
    	BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
    	if(!StringUtils.isBlank(q)){
        	map.put("q", q) ;
        }
    	if(!StringUtils.isBlank(ekind)){
    		boolQueryBuilder.must(termQuery("ekind" , ekind)) ;
        	map.put("ekind", ekind) ;
        }
    	
    	Iterable<Contacts> contactsList = contactsRes.findByQueryAndOrgi(null,super.getOrgi(request), null , null , false, boolQueryBuilder ,q , new PageRequest(super.getP(request) , super.getPs(request)));
    	MetadataTable table = metadataRes.findByTablenameIgnoreCase("uk_contacts") ;
    	List<Map<String,Object>> values = new ArrayList<Map<String,Object>>();
    	for(Contacts contacts : contactsList){
    		values.add(UKTools.transBean2Map(contacts)) ;
    	}

    	response.setHeader("content-disposition", "attachment;filename=UCKeFu-Contacts-"+new SimpleDateFormat("yyyy-MM-dd").format(new Date())+".xls");  

    	ExcelExporterProcess excelProcess = new ExcelExporterProcess( values, table, response.getOutputStream()) ;
    	excelProcess.process();
    	
        return ;
    }
    
    
    @RequestMapping("/embed/index")
    @Menu(type = "contacts" , subtype = "embed")
    public ModelAndView embed(ModelMap map , HttpServletRequest request , @Valid String q , @Valid String ckind, @Valid String ani) {
    	BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
    	if(!StringUtils.isBlank(q)){
        	map.put("q", q) ;
        }
    	if(!StringUtils.isBlank(ani)){
    		BoolQueryBuilder phoneBooleanQuery = QueryBuilders.boolQuery();
    		List<AgentUserContacts> agentUserContactsList = agentUserContactsRes.findByUseridAndOrgi(ani, super.getOrgi(request)) ;
    		if(agentUserContactsList != null && agentUserContactsList.size() > 0 && !org.apache.commons.lang3.StringUtils.isBlank(agentUserContactsList.get(0).getContactsid())) {
    			phoneBooleanQuery.should(termQuery("id" , agentUserContactsList.get(0).getContactsid())) ;
    		}
    		phoneBooleanQuery.should(termQuery("phone" , ani)) ;
    		phoneBooleanQuery.should(termQuery("mobilephone" , ani)) ;
    		boolQueryBuilder.must(phoneBooleanQuery) ;
        	map.put("ani", ani) ;
        }
    	map.addAttribute("contactsList", contactsRes.findByQueryAndOrgi( null,super.getOrgi(request),null , null , false, boolQueryBuilder ,q , new PageRequest(super.getP(request) , super.getPs(request)))) ;
    	
    	return request(super.createRequestPageTempletResponse("/apps/business/contacts/embed/index"));
    }
    
    @RequestMapping("/embed/add")
    @Menu(type = "contacts" , subtype = "embedadd")
    public ModelAndView embedadd(ModelMap map , HttpServletRequest request, @Valid String ani) {
    	if(!StringUtils.isBlank(ani)){
        	map.put("ani", ani) ;
        }
        return request(super.createRequestPageTempletResponse("/apps/business/contacts/embed/add"));
    }
    
    @RequestMapping(  "/embed/save")
    @Menu(type = "contacts" , subtype = "embedsave")
    public ModelAndView embedsave(HttpServletRequest request  , @Valid Contacts contacts, @Valid String ani) {
		contacts.setCreater(super.getUser(request).getId());
		contacts.setOrgi(super.getOrgi(request));
		contacts.setOrgan(super.getUser(request).getOrgan());
		contacts.setPinyin(PinYinTools.getInstance().getFirstPinYin(contacts.getName()));
		if(StringUtils.isBlank(contacts.getCbirthday())) {
			contacts.setCbirthday(null);
		}
		contactsRes.save(contacts) ;
        return request(super.createRequestPageTempletResponse("redirect:/apps/contacts/embed/index.html"+(!StringUtils.isBlank(ani) ? "?ani="+ani : null)));
    }
    
    @RequestMapping("/embed/edit")
    @Menu(type = "contacts" , subtype = "embededit")
    public ModelAndView embededit(ModelMap map , HttpServletRequest request , @Valid String id) {
    	map.addAttribute("contacts", contactsRes.findOne(id)) ;
        return request(super.createRequestPageTempletResponse("/apps/business/contacts/embed/edit"));
    }
    
    @RequestMapping(  "/embed/update")
    @Menu(type = "contacts" , subtype = "embedupdate")
    public ModelAndView embedupdate(HttpServletRequest request  , @Valid Contacts contacts) {
    	Contacts data = contactsRes.findOne(contacts.getId()) ;
    	if(data!=null){
	    	List<PropertiesEvent> events = PropertiesEventUtils.processPropertiesModify(request, contacts , data , "id" , "orgi" , "creater" ,"createtime" , "updatetime") ;	//记录 数据变更 历史
	    	if(events.size()>0){
	    		String modifyid = UKTools.getUUID() ;
	    		Date modifytime = new Date();
	    		for(PropertiesEvent event : events){
	    			event.setDataid(contacts.getId());
	    			event.setCreater(super.getUser(request).getId());
	    			event.setOrgi(super.getOrgi(request));
	    			event.setModifyid(modifyid);
	    			event.setCreatetime(modifytime);
	    			propertiesEventRes.save(event) ;
	    		}
	    	}
	    	
	    	contacts.setCreater(data.getCreater());
	    	contacts.setCreatetime(data.getCreatetime());
	    	contacts.setOrgi(super.getOrgi(request));
	    	contacts.setOrgan(super.getUser(request).getOrgan());
	    	contacts.setPinyin(PinYinTools.getInstance().getFirstPinYin(contacts.getName()));
	    	if(StringUtils.isBlank(contacts.getCbirthday())) {
				contacts.setCbirthday(null);
			}
	    	contactsRes.save(contacts);
    	}
    	
        return request(super.createRequestPageTempletResponse("redirect:/apps/contacts/embed/index.html"));
    }
    
    @RequestMapping("/updatemapping")
    @ResponseBody
    public void updatemapping(ModelMap map , HttpServletRequest request) {
    	contactsRes.updateMapping();
    }
    
    @RequestMapping("/batdel")
    @Menu(type = "contacts" , subtype = "contacts")
    public ModelAndView batdel(ModelMap map , HttpServletRequest request , HttpServletResponse response , @Valid String[] ids ,@Valid String ckind,@Valid String itemid) throws IOException {
    	if(ids!=null && ids.length > 0){
    		Iterable<Contacts> contactsList = contactsRes.findAll(Arrays.asList(ids)) ;
    		for(Contacts contacts :contactsList) {
    			contacts.setDatastatus(true);	
    		}
    		this.contactsRes.save(contactsList);
    	}
    	
    	 return request(super.createRequestPageTempletResponse("redirect:/apps/contacts/index.html?p="+request.getParameter("p")+"&itemid="+itemid));
    }
    
    @RequestMapping("/item/add")
    @Menu(type = "contacts" , subtype = "add")
    public ModelAndView itemadd(ModelMap map , HttpServletRequest request) {
    	map.addAttribute("contactsItemList", getContactsItem(super.getUser(request),super.getOrgi(request))) ;
        return request(super.createRequestPageTempletResponse("/apps/business/contacts/additem"));
    }
    
    @RequestMapping("/item/save")
    @Menu(type = "contacts" , subtype = "save")
    public ModelAndView itemsave(HttpServletRequest request  , @Valid ContactsItem contactsItem) {
    	ContactsItem item = null ;
    	if (!StringUtils.isBlank(contactsItem.getName())) {
    		item = new ContactsItem();
    		item.setCreater(super.getUser(request).getId());
    		item.setItemid(!StringUtils.isBlank(contactsItem.getParentid())?contactsItem.getParentid():item.getId());
    		item.setParentid(!StringUtils.isBlank(contactsItem.getParentid())?contactsItem.getParentid():null);
    		item.setName(contactsItem.getName());
    		item.setOrgi(super.getOrgi(request));
//    		item.setType(UKDataContext.ContactsItemType.CONTACTS.toString());
    		contactsItemRes.save(item) ;
		}
    	String id = item!=null?item.getId():"" ;
		return request(super.createRequestPageTempletResponse("redirect:/apps/contacts/index.html?itemid="+id));
    }
    
    @RequestMapping("/item/edit")
    @Menu(type = "contacts" , subtype = "add")
    public ModelAndView itemedit(ModelMap map , HttpServletRequest request,@Valid String id,@Valid String itemid) {
    	map.addAttribute("contactsItemList", getContactsItem(super.getUser(request),super.getOrgi(request))) ;
    	if (!StringUtils.isBlank(id)) {
			map.addAttribute("contactsItem", contactsItemRes.findByIdAndOrgi(id, super.getOrgi(request))) ;
		}
    	map.addAttribute("itemid", itemid) ;
        return request(super.createRequestPageTempletResponse("/apps/business/contacts/edititem"));
    }
    
    @RequestMapping("/item/update")
    @Menu(type = "contacts" , subtype = "save")
    public ModelAndView itemupdate(HttpServletRequest request  , @Valid ContactsItem contactsItem ,@Valid String itemid) {
    	ContactsItem item = null ;
    	if (!StringUtils.isBlank(contactsItem.getId()) && !StringUtils.isBlank(contactsItem.getName())) {
    		item = contactsItemRes.findByIdAndOrgi(contactsItem.getId(), super.getOrgi(request));
    		item.setItemid(!StringUtils.isBlank(contactsItem.getParentid())?contactsItem.getParentid():item.getId());
    		item.setParentid(!StringUtils.isBlank(contactsItem.getParentid())?contactsItem.getParentid():null);
    		item.setName(contactsItem.getName());
    		item.setUpdater(super.getUser(request).getId());
    		item.setUpdatetime(new Date());
    		contactsItemRes.save(item) ;
		}
    	String id = !StringUtils.isBlank(itemid)?itemid:item!=null?item.getId():"";
		return request(super.createRequestPageTempletResponse("redirect:/apps/contacts/index.html?itemid="+id));
    }
    
    @RequestMapping("/item/delete")
    @Menu(type = "contacts" , subtype = "contacts")
    public ModelAndView itemdelete(HttpServletRequest request ,@Valid String itemid) {
    	if(!StringUtils.isBlank(itemid)){
    		ContactsItem item = contactsItemRes.findByIdAndOrgi(itemid, super.getOrgi(request)) ;
    		if (item != null) {
    			List<ContactsItem> contactsItems = contactsItemRes.findByParentidAndOrgi(itemid, super.getOrgi(request)) ;
    			contactsItems.add(item) ;
    			if (contactsItems != null && contactsItems.size() > 0) {
    				List<String> contactidList = new ArrayList<String>();
					for(ContactsItem contactsItem : contactsItems) {
						if (!StringUtils.isBlank(contactsItem.getId())) {
							contactidList.add(contactsItem.getId()) ;
						}
					}
					List<ContactsItemAuthorize> contactsItemAuthorizeList = contactsItemAuthorizeRes.findByItemidInAndOrgi(contactidList, super.getOrgi(request)) ;
					if (contactsItemAuthorizeList != null && contactsItemAuthorizeList.size() > 0) {
						contactsItemAuthorizeRes.delete(contactsItemAuthorizeList);
					}
					contactsItemRes.delete(contactsItems);
				}
			}
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/apps/contacts/index.html"));
    }
    
    @RequestMapping("/item/auth")
    @Menu(type = "contacts" , subtype = "add")
    public ModelAndView itemauth(ModelMap map , HttpServletRequest request,@Valid String id) {
    	List<Organ> organList = organRes.findByOrgiAndOrgid(super.getOrgiByTenantshare(request),super.getOrgid(request)) ;
    	List<String> organstrList = new ArrayList<String>() ;
    	if (organList != null && organList.size() > 0) {
			for(Organ organ : organList) {
				organstrList.add(organ.getId());
			}
		}
    	map.addAttribute("contactsItemAuthorizeList",contactsItemAuthorizeRes.findByItemidAndOrgiAndDatastatus(id, super.getOrgid(request),false));
    	map.addAttribute("userList", userRes.findByOrganInAndDatastatus(organstrList, false) );
    	map.addAttribute("organList", organList);
		map.addAttribute("id", id) ;
        return request(super.createRequestPageTempletResponse("/apps/business/contacts/itemauth"));
    }
    
    @RequestMapping("/item/authsave")
    @Menu(type = "contacts" , subtype = "save")
    public ModelAndView authsave(HttpServletRequest request  , @Valid String id,@Valid String[] userid,@Valid String[] sup,@Valid String[] deluserid) {
    	if (!StringUtils.isBlank(id)) {
    		ContactsItem item = contactsItemRes.findByIdAndOrgi(id, super.getOrgi(request)) ;
    		if (item != null) {
    			List<ContactsItemAuthorize> tempContactsItemAuthorizeList = new ArrayList<ContactsItemAuthorize>();
    			if (deluserid != null && deluserid.length > 0) {
					List<ContactsItemAuthorize> delAuthorizes = contactsItemAuthorizeRes.findByUseridInAndItemidAndOrgiAndDatastatus(Arrays.asList(deluserid), id,  super.getOrgi(request),false) ;
					if (delAuthorizes != null && delAuthorizes.size() > 0) {
						for(ContactsItemAuthorize authorize : delAuthorizes) {
							authorize.setDatastatus(true);
							authorize.setCreater(super.getUser(request).getId());
							authorize.setCreatetime(new Date());
							tempContactsItemAuthorizeList.add(authorize);
						}
					}
				}
				if (userid != null && userid.length > 0) {
					List<User> userList = userRes.findByIdInAndOrgiAndDatastatus(Arrays.asList(userid), super.getOrgi(request), false) ;
					if (userList != null && userList.size() > 0) {
						for(User user : userList) {
							ContactsItemAuthorize cia = contactsItemAuthorizeRes.findByItemidAndUseridAndOrgi(item.getId(),user.getId(), super.getOrgi(request)) ;
							if (cia == null) {
								cia = new ContactsItemAuthorize();
							}
							cia.setDatastatus(false);
							cia.setCreater(super.getUser(request).getId());
							cia.setCreatetime(new Date());
							cia.setItemid(item.getId());
							cia.setOrgi(super.getOrgi(request));
							cia.setUserid(user.getId());
							cia.setSup(false);
							if (sup != null && sup.length > 0 && Arrays.asList(sup).contains(user.getId())) {
								cia.setSup(true);
							}
							tempContactsItemAuthorizeList.add(cia);
						}
					}
				}
				if (tempContactsItemAuthorizeList.size() > 0) {
					contactsItemAuthorizeRes.save(tempContactsItemAuthorizeList) ;
				}
			}
		}
		return request(super.createRequestPageTempletResponse("redirect:/apps/contacts/index.html?itemid="+id));
    }
    
    @RequestMapping("/item/user")
    @Menu(type = "callout" , subtype = "productcon")
    public ModelAndView callagentorganlist(ModelMap map , HttpServletRequest request , @Valid String organ, @Valid String itemid) {
    	map.addAttribute("contactsItemAuthorizeList",contactsItemAuthorizeRes.findByItemidAndOrgiAndDatastatus(itemid, super.getOrgid(request),false));
    	map.addAttribute("userList", userRes.findByOrganAndDatastatusAndOrgi(organ, false, super.getOrgi(request)) );
    	return request(super.createRequestPageTempletResponse("/apps/business/contacts/userlist")) ; 
    }
}