package com.ukefu.util.client;

public class UCKeFuClient implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1699520255768704808L;
	private String id ;
	private String name ;
	private String time ;
	private String ip ;
	private String userid ;
	private String extention ;
	private boolean disconnected ;
	
	public UCKeFuClient(String id , String name , String time ,String ip , String userid , String extention , boolean disconnected) {
		this.id = id ;
		this.name = name ;
		this.ip = ip ;
		this.extention = extention ;
		this.userid = userid ;
		this.disconnected=  disconnected ;
		this.time = time ;
				
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getExtention() {
		return extention;
	}
	public void setExtention(String extention) {
		this.extention = extention;
	}
	public boolean isDisconnected() {
		return disconnected;
	}
	public void setDisconnected(boolean disconnected) {
		this.disconnected = disconnected;
	}
}
